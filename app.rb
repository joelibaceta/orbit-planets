require 'sinatra' 
require 'sinatra/activerecord'
require 'json'
require "logger"
require 'sidekiq'
require 'sidekiq/api'

Dir[File.dirname(__FILE__) + '/lib/sample_system.rb'].each {|file| require file }

Dir[File.dirname(__FILE__) + '/app/models/*.rb'].each {|file| require file }


env_index = ARGV.index("-e")
env_arg = ARGV[env_index + 1] if env_index
env = env_arg || ENV["SINATRA_ENV"] || "development"


use ActiveRecord::ConnectionAdapters::ConnectionManagement 
databases = YAML.load_file("config/database.yml")
ActiveRecord::Base.establish_connection(databases[env])

Dir.mkdir('log') if !File.exists?('log') || !File.directory?('log')
ActiveRecord::Base.logger = Logger.new(File.open("log/#{env}.log", "a+"))

get '/sidekiq' do
  stats = Sidekiq::Stats.new
  workers = Sidekiq::Workers.new
  "
		<p>Processed: #{stats.processed}</p>
		<p>In Progress: #{workers.size}</p>
		<p>Enqueued: #{stats.enqueued}</p>
		<p><a href='/'>Refresh</a></p>
		<p><a href='/add_job'>Add Job</a></p>
		<p><a href='/sidekiq'>Dashboard</a></p>
		"
end

get '/clima' do
  day = params[:dia]
  forecast = Forecast.select([:day, :forecast]).where(day: day).last
  return forecast.to_json
end
