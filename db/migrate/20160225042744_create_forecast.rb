class CreateForecast < ActiveRecord::Migration
  def self.up
    create_table :forecasts do |t|
      t.column :day, :date, :null => false
      t.column :forecast, :string, :null => false
    end
  end

  def self.down
    drop_table :forecasts
  end
end

