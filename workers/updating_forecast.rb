load('lib/sample_system.rb')


class UpdatingForecast
  include SolarSystem

  def perform
    _newSystem = SampleSystem.new

    first_forecast  = Forecast.first
    last_forecast   = Forecast.last
    if (Date.parse(last_forecast.day) != Date.parse(Time.now))
      first_forecast.destroy!
      create({ day:       ( Date.parse(last_forecast.day).advance(days: 1)),
               forecast:  ASTRONOMIC_CALCULATOR.get_forecast_for_day(last_forecast.id + 1)})
    end
  end

end