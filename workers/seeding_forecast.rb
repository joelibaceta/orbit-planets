load('lib/sample_system.rb')

class SeedingForecast
  include Sidekiq::Worker
  include SolarSystem


  def perform
    _newSystem = SampleSystem.new

    start_date = Time.now
    end_date   = start_date.advance(years: 10)
    diff       = ((end_date - start_date) / 24 / 60 / 60)

    (0..diff).each do |day|
      Forecast.create({ day:       (Time.now.advance(days: day)),
                        forecast:  ASTRONOMIC_CALCULATOR.get_forecast_for_day(day).to_s})
    end
  end


end