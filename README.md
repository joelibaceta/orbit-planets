# Planetarium #

* The Ferengi Planet has an angular velocity of 1 degree/day clockwise. Radius from the sun is 500Km.
* The Betasoide Planet has an angular velocity of 3 degree/day clockwise. Radius from the sun is 2000Km. 
* The Vulcano Planet has an angular velocity of 3 degree/day counterclockwise. Radius from the sun is 1000Km. 
* All the orbits are circles.

## Setup ##

* ` gem install bundler`
* ` bundle install `
* ` rake db:create `
* ` rake db:migrate ` 
* ` rake db:seed `

## Run Sinatra server ##

` ruby app.rb `

### WebServices

#### Clima

**Example:**

* http://localhost:4567/clima?dia=2016-09-23
* ` {"id":5794,"day":"2016-09-23","forecast":"Normal"} `

#### Sidekiq

Get the queue status

*http://localhost:4567/sidekiq


## Rake task (Console Application)

### forecast:get_ten_years_brief ###
Get the forecast for the next ten years.

**Run** ` rake forecast:get_ten_years_brief `

* ` {"Optimum"=>21, "Normal"=>2402, "Rainy"=>1167, "RainyMaximum"=>41, "Drought"=>20} `

## Run Graphical Mode Simulator 

` ruby graphic.rb `

![Graphical Simulator](http://s22.postimg.org/aszmxvf6p/Captura_de_pantalla_2016_02_25_a_las_4_11_28_p_m.png) 


## Mathematical Fundamentals


### Calculation of inline aligment planets

Find the line equation constants

![Line Equation](http://s14.postimg.org/slc8u0ufl/MSP2011g35a2f02f9h1cf9000068e05b3gh15c8154.gif)

### Calculation of triangle aligment planets

### Calculation of triangle aligment planets and sun inner

### Calculation of maximum perimeter