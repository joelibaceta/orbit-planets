require 'gosu'

load 'lib/sample_system.rb'

class Circle
  
end

class MyWindow < Gosu::Window
  include SolarSystem
  
  @@time = 0
  
  def initialize
    super(1400, 1000)
    self.caption = 'Forecast Calculator'

    @newSystem = SampleSystem.new # Initialize basic planets

    @f = Gosu::Font.new(20)
    @b = Gosu::Font.new(20)
    @v = Gosu::Font.new(20)
    
    @status = Gosu::Font.new(20)
    @sun = Gosu::Font.new(50)
    
    @mathematical_data = Gosu::Font.new(20)
     
  end
  
  def update
    @@time += 0.1
    self.caption = "Day #{@@time.round(0)}"
    
    
  end

  def draw
    f_pos = @newSystem.ferengi.actual_position(@@time.round)
    b_pos = @newSystem.betasoide.actual_position(@@time.round)
    v_pos = @newSystem.vulcano.actual_position(@@time.round)

    distribution = ASTRONOMIC_CALCULATOR.get_triad_distribution(@@time.round)

    @f.draw("o Ferengi#{f_pos.inspect}",
            f_pos[0] / 4 + 700, f_pos[1] / 4 + 500,
            2, 1.0, 1.0, 0xff_ff0000)
    @b.draw("o Betasoide#{b_pos.inspect}",
            b_pos[0] / 4 + 700, b_pos[1] / 4 + 500,
            2, 1.0, 1.0, 0xff_0000ff)
    @v.draw("o Vulcano#{v_pos.inspect}",
            v_pos[0] / 4 + 700, v_pos[1] / 4 + 500,
            2, 1.0, 1.0, 0xff_00ff00)

    draw_triangle(f_pos[0] / 4 + 700, f_pos[1] / 4 + 500, 0xff_ff0000,
                  b_pos[0] / 4 + 700, b_pos[1] / 4 + 500, 0xff_0000ff,
                  v_pos[0] / 4 + 700, v_pos[1] / 4 + 500, 0xff_00ff00)

    @sun.draw("o", 700, 500, 2, 1.0, 1.0, 0xff_ffff00)
    @status.draw("Day #{@@time.round}, Forecast: #{ASTRONOMIC_CALCULATOR.get_forecast_for_day(@@time.round(0))}",
                 10, 10, 2, 1.0, 1.0, 0xff_ffff00)
    @mathematical_data.draw("Distribution Propierties : #{distribution.properties}",
                            10, 30, 2, 1.0, 1.0, 0xff_ffff00)
  end
end

window = MyWindow.new
window.show