# Planet definition

class Planet

  attr_reader :vector_velocity
  attr_reader :angular_velocity
  attr_reader :radius
  attr_reader :name
  
  def initialize(name, w, r, spin)
    @name             = name
    @angular_velocity = w * spin
    @radius           = r # in radians
  end
  
  def distance(time); @angular_velocity * time; end # in kms
  def angular_swept(time); @angular_velocity * time; end # in radians
  def speed; @angular_velocity * @radius; end
  def period; (Math::PI * 2) / @angular_velocity; end
  
  def actual_position(time)
    i = @radius * Math.sin(@angular_velocity * time)
    j = @radius * Math.cos(@angular_velocity * time)
    [i.round(3), j.round(3)]
  end
end