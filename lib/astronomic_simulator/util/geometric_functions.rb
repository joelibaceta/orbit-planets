module GeometricFunctions
  
  def distribution_factory(x1, y1, x2, y2, x3, y3)
    line1, line2 = Line.new(x1, y1, x2, y2), Line.new(x2, y2, x3, y3)
    line1.m.abs == line2.m.abs ? line1 : Triangle.new(x1, y1, x2, y2, x3, y3)
  end
  
  class Triangle
    attr_reader :v1, :v2, :v3, :x1, :y1, :x2, :y2, :x3, :y3
    attr_reader :perimeter
    
    def initialize(x1, y1, x2, y2, x3, y3)
      @x1, @y1, @x2, @y2, @x3, @y3 = x1, y1, x2, y2, x3, y3
      set_perimeter(x1, y1, x2, y2, x3, y3)
    end
    
    def is_dot_in?(d)
      a = ((@y2 - @y3) * (d[:x] - @x3) + (@x3 - @x2) * (d[:y] - @y3)) / ((@y2 - @y3) * (@x1 - @x3) + (@x3 - @x2) * (@y1 - @y3))
      b = ((@y3 - @y1) * (d[:x] - @x3) + (@x1 - @x3) * (d[:y] - @y3)) / ((@y2 - @y3) * (@x1 - @x3) + (@x3 - @x2) * (@y1 - @y3))
      c = 1 - a - b
      0 <= a && a <= 1 && 0 <= b && b <= 1 && 0 <= c && c <= 1
    end
    
    def set_perimeter(x1, y1, x2, y2, x3, y3)
      a = Math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
      b = Math.sqrt((x3 - x2)**2 + (y3 - y2)**2)
      c = Math.sqrt((x1 - x3)**2 + (y1 - y3)**2)
      @perimeter = a + b + c
    end
    
    def properties; { class: self.class.name, perimeter: @perimeter }; end
    

  end
  
  class Line
    attr_reader :m, :b
    
    @@validator = Proc.new { |y, m, x, b| (y.round(1) == (m * x + b).round(1)) }
    def initialize(x1, y1, x2, y2); set_equation_constants(x1, y1, x2, y2); end
    def are_dots_in?(*dots); dots.map { |d| @@validator.call(d[:y], @m, d[:x], @b) }.all?; end
    def properties; { class: self.class.name, equation: "y = #{@m}.x + #{@b}" }; end
    
    private

    def set_equation_constants(x1, y1, x2, y2)
      @m = ((y2 - y1) / (x2 - x1)).round(2)
      @b = (y2 - (@m * x2)).round(2)
    end

  end
  
end


