load 'lib/astronomic_simulator/util/geometric_functions.rb'

class AstronomicCalculator
  include GeometricFunctions

  SUN_COORDS = { x: 0, y: 0 }

  attr_accessor :planets

  def get_forecast(days, planets = @planets)
    (0..days).map { |day|  get_forecast_for_day(day, planets) }
  end

  def get_triad_distribution(day, planets = @planets)
    if planets.count == 3
      x1, y1 = planets[0].actual_position(day)
      x2, y2 = planets[1].actual_position(day)
      x3, y3 = planets[2].actual_position(day)
      return distribution_factory(x1, y1, x2, y2, x3, y3)
    else
      msg = "3 planets are required to use this function, using #{planets.count}"
      raise ArgumentError.new(msg)
    end
  end

  def get_forecast_for_day(day, planets = @planets)

    # Support only trhee planets by now

    distribution = get_triad_distribution(day, planets)

    #Apply Forecast calculation rules
    case distribution.class.name
      when 'GeometricFunctions::Line' then
        sun_is_inline = distribution.are_dots_in?(SUN_COORDS)
        return sun_is_inline ? 'Drought' : 'Optimum'
      when 'GeometricFunctions::Triangle' then
        sun_is_in_area = distribution.is_dot_in?(SUN_COORDS)
        return sun_is_in_area ? "Rainy#{is_maximum_perimeter?(day, planets) ? 'Maximum' : '' }" : 'Normal'

    end

  end

  private

  def is_maximum_perimeter?(t, planets = @planets)

    #Calculating Critical points of Perimeter Functions

    r1, r2, r3 = planets[0].radius, planets[1].radius, planets[2].radius
    w1, w2, w3 = planets[0].angular_velocity, planets[1].angular_velocity, planets[2].angular_velocity

    a = (r1 * r2 * (w1 - w2) * Math.sin(t * (w1 - w2))) / (Math.sqrt( -2 * r1 * r2 * Math.cos(t * (w1-w2)) + r2**2 + r1**2))
    b = (r3 * r1 * (w3 - w1) * Math.sin(t * (w3 - w1))) / (Math.sqrt( -2 * r3 * r1 * Math.cos(t * (w3-w1)) + r1**2 + r3**2))
    c = (r3 * r2 * (w3 - w2) * Math.sin(t * (w3 - w2))) / (Math.sqrt( -2 * r3 * r2 * Math.cos(t * (w3-w2)) + r2**2 + r3**2))

    # If is aprox 0, has a maximum value along the day
    (a + b + c).round <= 1 && (a + b + c).round >= -1 # if is maiximum
  end

end
