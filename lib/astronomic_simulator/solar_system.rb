load 'lib/astronomic_simulator/planet.rb'
load 'lib/astronomic_simulator/util/astronomic_calculator.rb'

module SolarSystem

  @@planets = []
  ASTRONOMIC_CALCULATOR = AstronomicCalculator.new

  def new_planet(planet)
    @@planets << planet
    planet
  end

  def all_created_planets; @@planets; end

end