load 'lib/astronomic_simulator/solar_system.rb'

class SampleSystem
  include SolarSystem

  attr_reader :ferengi, :betasoide, :vulcano

  def initialize
    @ferengi    = new_planet(Planet.new('Ferengi', 1.0 * (Math::PI / 180), 500.0, 1.0))
    @betasoide  = new_planet(Planet.new('Betasoide', 3.0 * (Math::PI / 180), 2000.0, 1.0))
    @vulcano    = new_planet(Planet.new('Vulcano', 5.0 * (Math::PI / 180), 1000.0, -1.0))
    SolarSystem::ASTRONOMIC_CALCULATOR.planets = all_created_planets
  end

end