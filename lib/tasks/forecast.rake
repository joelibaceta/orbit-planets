load 'lib/sample_system.rb'

include SolarSystem

namespace :forecast do
  desc "Calcular resumen prediccion para los proximos 10 años"
  task :get_ten_years_brief do
    forecast_hash = Hash.new
    _newSystem = SampleSystem.new
    forecast = ASTRONOMIC_CALCULATOR.get_forecast(10 * 365)
    forecast.map{|f| forecast_hash[f] = forecast_hash[f].to_i + 1 }
    p forecast_hash
  end
end